package com.m3.training.cardgames;

public class Game {

	
	public static void main(String[] args) {
		int handSize = 3;
		Hand player1 = new Hand(handSize);
		Hand player2 = new Hand(handSize);

		// TODO Debugger
		// TODO equals
		// TODO hashcode

		for (int index = 0; index < handSize; index++) {
			player1.add(Card.createCard());
			player2.add(Card.createCard());
		}
		
		Card card1 = new Card(2, 0);
		Card card2 = new Card(1, 1);
		System.out.println("Card1 " + card1 + " Card2 " + card2);	
		System.out.println("Are they the same value? " + card1.equals(card2));
		Card highCard = card1.compareTo(card2) > 0 ? card1 : card2;
		System.out.println("Which is higher, card1 or card2? " + highCard);
				
		
		
		System.out.println("Player 1 hand is " + player1);
		System.out.println("Player 2 hand is " + player2);	
		

	}

}
