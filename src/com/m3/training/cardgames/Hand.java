package com.m3.training.cardgames;

import java.util.ArrayList;
import java.util.List;

public class Hand {

	private List<Card> hand; 
	private int size;
	
	public Hand(int size) {
		hand = new ArrayList<>();
		this.size = size;
	}

	public boolean add(Card card) {
		return hand.add(card);
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Card card : hand) {
			result.append(card.toString());
			result.append(" ");
		}
		return result.toString();
	}
	
	
}
