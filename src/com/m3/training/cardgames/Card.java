package com.m3.training.cardgames;

import java.util.HashMap;
import java.util.Random;

// extend this class with a class for French style
// extend this class with a class for Pokemon

public class Card {
	
	private static final HashMap<Integer, String> suitMap = new HashMap<>();
	private static final HashMap<Integer, String> rankMap = new HashMap<>();

	{	
		suitMap.put(0, "Spades");
		suitMap.put(1, "Diamond");
		
		rankMap.put(1, "Ace");
		rankMap.put(2, "Two");	
	}
	
	private int suit;
	private int rank;
	
	
	public Card(int rank, int suit) {
		super();
		this.setRank(rank);
		this.setSuit(suit);
	}
	
	public int getSuit() {
		return suit;
	}
	public void setSuit(int suit)
	{
		if (suit < 0 || suit > 3) {
			throw new IllegalArgumentException("Valid suits are 0 through 3.");
		}
		this.suit = suit;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		if (rank < 1 || rank > 13) {
			throw new IllegalArgumentException("Valid ranks are 1 through 13.");
		}
		this.rank = rank;
	}
	
	
	@Override
	public String toString() {
		return Card.rankMap.get(this.getRank()) + " of " +
				Card.suitMap.get(this.getSuit());
	}
	
	public static Card createCard() {
		Random ran = new Random();
		int suit = ran.nextInt(2);
		int rank = ran.nextInt(2) + 1;
		return new Card(rank, suit);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rank;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Card other = (Card) obj;
		if (rank != other.rank)
			return false;
		return true;
	}
	
	public int compareTo(Card other) {
		if (this.equals(other)) {
			return 0;
		}
		
		return this.getRank() - other.getRank();
		
	}
	
	
	
}
